FROM python:3.6-stretch 
MAINTAINER Amit Vyas (amitvyas007@gmail.com)
COPY ./requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt
COPY . /apps/middleman/
WORKDIR /apps/middleman/middleman/
EXPOSE 8000
CMD python manage.py makemigrations; python manage.py migrate; gunicorn middleman.wsgi:application --access-logfile '-' -w 3 -k gevent --bind=0.0.0.0:8000