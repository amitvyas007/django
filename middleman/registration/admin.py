from django.contrib import admin

# Register your models here.
from registration.models import Registration

class registration_admin(admin.ModelAdmin):
    search_fields = ['id', 'status']
    readonly_fields=('id', 'created_at', 'updated_at')
    
    # uncomment below fields line to display only few columns
    # fields = ('id','user_id', 'status', 'created_at', 'updated_at')

    fieldsets = (
        (None, {
            'fields': ('id', 'name', 'status')
        }),
        ('Additional Info', {
            'classes': ('collapse',),
            'fields': ('created_at', 'updated_at'),
        }),
    )

    list_display = ('id', 'name', 'status', 'created_at', 'updated_at')
    def get_ordering(self, request):
        return ['-created_at']
    pass

admin.site.register(Registration, registration_admin)