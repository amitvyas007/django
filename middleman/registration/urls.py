from django.urls import path
from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'^get$', views.registration_get, name="registration_get"),
    url(r'^getbyid$', views.registration_getbyid, name="registration_getbyid"),
    url(r'^upsert$', views.registration_upsert, name="registration_upsert"),
    url(r'^status$', views.registration_update_status, name="registration_update_status"),
]