from registration.models import Registration
from registration.serializers import RegistrationSerializer
from django.core import serializers
import os
import requests
import json
import uuid
import sys
from datetime import datetime, timedelta
from django.db.models import Count, Sum
from django.db.models.query import QuerySet
from . import repository


DEBUG = False
try:
    HOST_ENV = os.environ['CP_MIDDLEMAN_ENV']
    print('Environment is', HOST_ENV)
    if str(HOST_ENV).upper() == 'DEV':
        DEBUG = True
except Exception as e:
    print(e)

print('DEBUG --> ', DEBUG)

def Print(*objects):
    if DEBUG:
        MESSAGE = ''
        for obj in objects:
            MESSAGE += str(obj)
        # print(MESSAGE, end='\n\n\n\n')
        print(MESSAGE, sep=' ', end='\n\n', file=sys.stderr)



Print('API Running in Debug Mode')

# Initial Decided Valid status combination in tuple format
# (Received -> Error, Received -> Resolved, Received ->Unresolved, Resolved -> Processed, Resolved -> Error, Unresolved -> Error)
# initializing tuple of Status
tuple_status = (('CREATED', 'ERROR'), ('CREATED', 'UPDATED'))

Print('Tuple of Status --> ', tuple_status)




# Registration get method, which simply get the params 
# and fetch the data without restriction
def registration_get(payload):
    response = {'success': False}
    Print('Processing fetch registration request with payload --> ', payload)

    try:
        registrations = repository.registration_get(payload)
        if(registrations != None and registrations != {}):
            response['data'] = {}
            response['data'] = registrations
            response['success'] = True
        else:
            response['error_code'] = 'registration-4'
            response['error'] = 'No Record(s) found!!!'
    except Exception as e:
        print('Exception occured while fetching data')
        print(e)
        response['error_code'] = 'registration-1'
        response['error'] = str(e)

    Print('Registration get final response --> ', response)
    return response


# Registration get by id method, which simply get the params 
# and fetch the single registration without restriction
def registration_getbyid(id):
    response = {'success': False}
    Print('Processing get registration with id --> ', id)

    try:
        registration = repository.registration_getbyid(id)
        
        if(registration != None and registration != {}):
            response['data'] = {}
            response['data'] = registration
            response['success'] = True
        else:
            response['error_code'] = 'registration-4'
            response['error'] = 'No Record(s) found!!!'
    except Exception as e:
        print('Exception occured while fetching data')
        print(e)
        response['error_code'] = 'registration-1'
        response['error'] = str(e)

    Print('Registration get by id final response --> ', response)
    
    return response



# Registration upsert method which impose few validations before actual upsert
# this method supports both insert and update based on the ID provided, 
# if id isn't passed it will going to create the new registration record
def registration_upsert(payload):
    response = {'success': False}
    
    Print('Upsert request --> ', payload)

    if payload == None or payload == {}:
        response["error_code"] = "registrations-2"
        response["error"] = 'Invalid JSON'
    elif 'id' in payload:
        response_status = registration_update_status(payload['id'], payload['status'])
        if response_status == None or response_status == {}:
            response["error_code"] = "registrations-8"
            response["error"] = 'Invalid Status'
        elif 'success' in response_status and response_status['success'] == False:
            return response_status
    
    response_upsert = repository.registration_upsert(payload)
    Print('Upsert response after update --> ', response_upsert)

    if response_upsert != None and 'success' in response_upsert and response_upsert['success'] == True:
        if 'data' in response_upsert and response_upsert['data'] != {}:
            if 'id' in response_upsert['data']:
                id = response_upsert['data']['id']
                Print('Registration ID got generated --> ', id)
            if 'mode' in response_upsert['data']:
                mode = response_upsert['data']['mode']
                Print('Registration mode --> ', mode)
            response['data']['mode'] = mode

            if id == None or id == '':
                response['error_code'] = 'registration-5'
                response['error'] = 'Unable to process your request!!!'
                return response

            
            # Getting registration record by id
            registration = repository.registration_getbyid(id)

            Print('Registration got based on id --> ', registration)


            registration = repository.registration_getbyid(id)

            response['data']['registration'] = registration
    
    if 'error' not in response:
        response['success'] = True

    return response



# This method updates the status of the record
# This method will verify the registration should be valid status
def registration_update_status(id, status):
    response = {'success': False}

    CURRENT_STATUS = 'CREATED'

    Print('Going to update the status of Registration id --> ', id, ' status --> ', status)

    try:
        registration_response = registration_getbyid(id)

        Print('Registration update status get registration response --> ', registration_response)

        if registration_response != None and 'success' in registration_response and registration_response['success'] == True:
            registration = registration_response['data']
            CURRENT_STATUS = registration['status']

        if CURRENT_STATUS == status:
            response['success'] = True
            return response
        # to verify if the status is being passed is correct or not
        elif _validate_status(from_status=CURRENT_STATUS, desired_status=status): 
            response = repository.registration_update_status(id, status)
        else : 
            error_msg = 'Error!!! Invalid Registration status. From status ' + CURRENT_STATUS + ' to ' + status
            Print(error_msg)
            response['error_code'] = 'registration-7'
            response['error'] = error_msg
    except Exception as e:
        response['error_code'] = 'registration-1'
        response['error'] = str(e)
        print(e)

    Print('Registration update status FINAL registration response --> ', response)
    return response



def _validate_status(from_status, desired_status):
    status = False

    try:
        if (any(i[0] == str(from_status).upper() and i[1] == str(desired_status).upper() for i in tuple_status)): 
            status = True
    except Exception as e:
        print(e)
    
    return status
