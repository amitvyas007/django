from django.db import models
import time
import uuid


def uuid_str():
    return str(uuid.uuid4())


class Registration(models.Model):
    id = models.CharField(primary_key=True, default=uuid_str, editable=False, max_length=50)
    name = models.CharField(max_length=500)

    status = models.CharField(max_length=100, default="CREATED")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # class Meta:
    #     unique_together = (('tx_hash', 'coin', 'address'))

    def __str__(self):
        return (self.id)

