from registration.models import Registration
from registration.serializers import RegistrationSerializer
from django.core import serializers
import os
import requests
import json
import uuid
import sys
import random
import string

DEBUG = False
try:
    HOST_ENV = os.environ['CP_MIDDLEMAN_ENV']
    print('Environment is', HOST_ENV)
    if str(HOST_ENV).upper() == 'DEV':
        DEBUG = True
except Exception as e:
    print(e)

print('DEBUG --> ', DEBUG)

def Print(*objects):
    if DEBUG:
        MESSAGE = ''
        for obj in objects:
            MESSAGE += str(obj)
        # print(MESSAGE, end='\n\n\n\n')
        print(MESSAGE, sep=' ', end='\n\n', file=sys.stderr)


def registration_getbyid(id):
    registration = None
    resp = None
    try:
        try:
            resp = Registration.objects.get(id=id)
        except Registration.DoesNotExist:
            resp = None
        if resp != None:
            serializer = RegistrationSerializer(resp)
            registration = serializer.data
    except Exception as e:
        print('Exception occurred while getting record for registration id --> ', id)
        print(e)
        raise(e)    

    Print('Fetch from id response --> ', registration)
    
    return registration

def registration_get(payload):
    registrations = None
    Print('Fetching data of registrations against payload --> ', payload)
    resp = None
    try:
        resp = Registration.objects.filter(**payload).order_by('-created_at')
        Print('Fetch data response --> ', resp)
        if resp != None and resp.exists():
            serializer = RegistrationSerializer(resp, many=True)
            registrations = serializer.data
    except Exception as e:
        print('Exception occurred while getting record for registration request --> ', payload)
        print(e)
        raise(e)

    Print('Fetch data final response --> ', registrations)
    
    return registrations


def registration_upsert(payload):
    response = {'success': False, 'data': {}}
    
    Print('Upsert Payload ---> ', payload)

    if payload != None and 'id' in payload and payload['id'] != '':
        if DEBUG:
            response['data']['mode'] = 'UPDATE'
        registration = registration_getbyid(payload['id'])

        Print('Upsert get by id --> ', registration)

        if registration != None and registration != {}:
            if 'status' in payload:
                del payload['status']
        else:
            response['error'] = 'Invalid Request!!'
            response['error_code'] = 'registration-2'
            return response
        registration = Registration.objects.get(id=payload['id'])
        serializer = RegistrationSerializer(registration, data=payload, partial=True)
    else:
        if DEBUG:
            response['data']['mode'] = 'INSERT'
        serializer = RegistrationSerializer(data=payload)
    
    Print('Upsert Payload with registration_id --> ', payload)
    
    Print('Serializer --> ', serializer)
    if serializer.is_valid():
        try:
            output_id = serializer.save()
            response['data']['id'] = str(output_id)
        except Exception as e:
            print(e)
            response['error_code'] = 'registration-1'
            response['error'] = str(e)
    else:
        response['error_code'] = 'registration-2'
        response['error'] = 'Validation failed!!!'
    
    if 'error' not in response:
        response['success'] = True

    Print('Upsert Final Response --> ', response)
    return response


def registration_update_status(id, status):
    response = {'success': False, 'data': {}}

    Registration.objects.filter(id=id).update(status=status)
    
    response['success'] = True
    return response
