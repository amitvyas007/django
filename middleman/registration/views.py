from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.http import HttpResponseBadRequest, HttpResponseServerError, JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from . import helper
import traceback
import json
from registration.models import Registration
from registration.serializers import RegistrationSerializer


@api_view(['POST'])
def registration_get(request):
    response = {'success': False}

    try:
        payload = request.data
        print(payload)
        response = helper.registration_get(payload)
    except KeyError as e:
        traceback.print_exc()
        response["error_code"] = "registrations-3"
        response["error"] = 'Required field %s is missing' % e
    except json.decoder.JSONDecodeError as e:
        traceback.print_exc()
        response["error_code"] = "registrations-2"
        response["error"] = 'Invalid JSON. %s' % e
    except Exception as e:
        traceback.print_exc()
        response["error_code"] = "registrations-1"
        response["error"] = str(e)
    
    return JsonResponse(response)


@api_view(['GET'])
def registration_getbyid(request):
    response = {'success': False}

    try:
        id = request.GET.get('id')
        print('Fetching record for id --> ', id)
        response = helper.registration_getbyid(id)
    except KeyError as e:
        traceback.print_exc()
        response["error_code"] = "registrations-3"
        response["error"] = 'Required field %s is missing' % e
    except json.decoder.JSONDecodeError as e:
        traceback.print_exc()
        response["error_code"] = "registrations-2"
        response["error"] = 'Invalid JSON. %s' % e
    except Exception as e:
        traceback.print_exc()
        response["error_code"] = "registrations-1"
        response["error"] = str(e)
    
    return JsonResponse(response)


@api_view(['POST'])
def registration_upsert(request):
    response = {'success': False}

    try:
        payload = request.data
        print(payload)
        response = helper.registration_upsert(payload)
    except KeyError as e:
        traceback.print_exc()
        response["error_code"] = "registrations-3"
        response["error"] = 'Required field %s is missing' % e
    except json.decoder.JSONDecodeError as e:
        traceback.print_exc()
        response["error_code"] = "registrations-2"
        response["error"] = 'Invalid JSON. %s' % e
    except Exception as e:
        traceback.print_exc()
        response["error_code"] = "registrations-1"
        response["error"] = str(e)
    return JsonResponse(response)


@api_view(['POST'])
def registration_update_status(request):
    response = {'success': False}

    try:
        payload = request.data
        print(payload)
        response = helper.registration_update_status(payload)
    except KeyError as e:
        traceback.print_exc()
        response["error_code"] = "registrations-3"
        response["error"] = 'Required field %s is missing' % e
    except json.decoder.JSONDecodeError as e:
        traceback.print_exc()
        response["error_code"] = "registrations-2"
        response["error"] = 'Invalid JSON. %s' % e
    except Exception as e:
        traceback.print_exc()
        response["error_code"] = "registrations-1"
        response["error"] = str(e)
    return JsonResponse(response)

