### django boilerplate code




### Useful links
https://docs.djangoproject.com/en/2.2/intro/tutorial01/
http://127.0.0.1:8000/admin/login/?next=/admin/


Possible state changes of Registration: 
# Initial Decided Valid status combination in tuple format
# (CREATED -> Error, CREATED -> UDPATED)
# initializing tuple of Status
```
tuple_status = (('CREATED', 'ERROR'), ('CREATED', 'UPDATED'))
```

## Status of Deposit Controller Explained
1.  RECEIVED    --> This will be the default status for registration controller, Caller of Upsert API doesn't need to send the status from their end
2.  ERROR       --> Error occured at any stage of the registration controller lifecycle
3.  UPDATED   --> FINAL STATUS


# GET /registration/getbyid?id=9903aa61-50bd-49b6-ab31-ea62a59b38ba

## Sample success response

```JSON
{
    "success": true,
    "data": {
        "id": "9903aa61-50bd-49b6-ab31-ea62a59b38ba",
        "name": "rahul",
        "status": "CREATED",
        "created_at": "2019-12-09T18:03:07.365790Z",
        "updated_at": "2019-12-09T18:03:07.365814Z"
    }
}
```

## Sample failed response
```JSON
{
    "success": false,
    "error_code": "registration-4",
    "error": "No Record(s) found!!!"
}
```

# POST /registration/upsert
## Sample Request

```JSON
{
    "id": "9903aa61-50bd-49b6-ab31-ea62a59b38ba",
    "name": "Amit",
    "status": "UPDATED"
}
```

## Sample success response

```JSON
{
    "success": true,
    "data": {
        "mode": "UPDATE",
        "registration": {
            "id": "9903aa61-50bd-49b6-ab31-ea62a59b38ba",
            "name": "Rahul",
            "status": "UPDATED",
            "created_at": "2019-12-09T18:03:07.365790Z",
            "updated_at": "2019-12-09T19:14:24.655481Z"
        }
    }
}
```

## Sample failed response
```JSON
{
    "success": false,
    "error_code": "registration-7",
    "error": "Error!!! Invalid Registration status. From status UPDATED to SOMETHING"
}
```


# POST /registration/get

## Sample Request

```JSON
{
    "name": "amit"
}
```

## Sample success response
```JSON
{
    "success": true,
    "data": [
        {
            "id": "c153f83e-b265-474f-8ab3-f37b687dfa86",
            "name": "amit",
            "status": "CREATED",
            "created_at": "2019-12-09T18:03:02.314043Z",
            "updated_at": "2019-12-09T18:03:02.314069Z"
        }
    ]
}
```

## Sample failed response
```JSON
{
    "success": false,
    "error_code": "registration-4",
    "error": "No Record(s) found!!!"
}
```