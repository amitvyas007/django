#!/bin/bash
export CP_MIDDLEMAN_DB_HOST="127.0.0.1"
export CP_MIDDLEMAN_DB_DB="middleman"
export CP_MIDDLEMAN_DB_USERNAME="postgres"
export CP_MIDDLEMAN_DB_PASSWD="somepassword"
export CP_MIDDLEMAN_DB_PORT="5432"
export CP_MIDDLEMAN_ENV="dev"

case "$1" in
install)
    brew install pipenv
    ;;
setup)
    pipenv shell
    ;;
dev) 
    python ./middleman/manage.py makemigrations; python ./middleman/manage.py migrate; python ./middleman/manage.py runserver;
    ;;
user) 
    python ./middleman/manage.py createsuperuser;
    ;;
update-requirements) 
    pipenv lock -r > requirements.txt
    ;;
*)
    echo "Invalid argument. Supported arguments are 'install', 'setup', 'update-requirements' and 'dev'"
esac